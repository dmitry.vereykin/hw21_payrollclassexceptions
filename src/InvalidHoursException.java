/**
 * Created by Dmitry on 7/13/2015.
 */
public class InvalidHoursException extends Exception{
    public InvalidHoursException(double hours) {
        super("Invalid number of hours: " + hours);
    }
}
